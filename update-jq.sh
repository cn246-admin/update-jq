#!/bin/sh

# Description: Download, verify and install jq binary on Linux and Mac
# Author: Chuck Nemeth
# https://jqlang.github.io/jq/

# Colored output
code_err() { tput setaf 1; printf '%s\n' "$*" >&2; tput sgr0; }
code_grn() { tput setaf 2; printf '%s\n' "$*"; tput sgr0; }
code_yel() { tput setaf 3; printf '%s\n' "$*"; tput sgr0; }

# Define funciton to delete temporary install files
clean_up() {
  printf '%s\n' "[INFO] Cleaning up install files"
  cd && rm -rf "${tmp_dir}"
}

# OS Check
archi=$(uname -sm)
case "$archi" in
  Darwin\ arm64)
    jq_binary="jq-macos-arm64" ;;
  Darwin\ x86_64)
    jq_binary="jq-macos-amd64" ;;
  Linux\ armv[5-9]* | Linux\ aarch64*)
    jq_binary="jq-linux-arm64" ;;
  Linux\ *64)
    jq_binary="jq-linux-amd64" ;;
  *)
    code_err "[ERROR] Unsupported OS. Exiting"; exit 1 ;;
esac

# Variables
bin_dir="$HOME/.local/bin"
man_dir="$HOME/.local/share/man/man1"

if command -v jq >/dev/null 2>&1; then
  jq_installed_version="$(jq --version)"
else
  jq_installed_version="Not Installed"
fi

jq_version_number="$(curl -Ls https://api.github.com/repos/jqlang/jq/releases/latest | \
                     awk -F': ' '/tag_name/ { gsub(/\"|jq-|\,/,"",$2); print $2 }')"
jq_version="jq-${jq_version_number}"
jq_url="https://github.com/jqlang/jq/releases/download/${jq_version}"
jq_man_url="https://raw.githubusercontent.com/jqlang/jq/master/jq.1.prebuilt"
jq_man="jq.1"

sum_file="sha256sum.txt"

# PATH Check
case :$PATH: in
  *:"${bin_dir}":*)  ;;  # do nothing
  *)
    code_err "[ERROR] ${bin_dir} was not found in \$PATH!"
    code_err "Add ${bin_dir} to PATH or select another directory to install to"
    exit 1 ;;
esac

if [ "${jq_version}" = "${jq_installed_version}" ]; then
  printf '%s\n' "Installed Verision: ${jq_installed_version}"
  printf '%s\n' "Latest Version: ${jq_version}"
  code_yel "[INFO] Already using latest version. Exiting."
  exit
else
  printf '%s\n' "Installed Verision: ${jq_installed_version}"
  printf '%s\n' "Latest Version: ${jq_version}"
  tmp_dir="$(mktemp -d /tmp/jq.XXXXXXXX)"
  trap clean_up EXIT
  cd "${tmp_dir}" || exit
fi

# Download
printf '%s\n' "[INFO] Downloading the jq binary and verification files"
curl -sL -o "${tmp_dir}/${jq_binary}" "${jq_url}/${jq_binary}"
curl -sL -o "${tmp_dir}/${sum_file}" "${jq_url}/${sum_file}"

# Verify shasum
printf '%s\n' "[INFO] Verifying ${jq_binary}"
if ! shasum -qc --ignore-missing "${sum_file}"; then
  code_err "[ERROR] Problem with checksum!"
  exit 1
fi

# Create directories
[ ! -d "${bin_dir}" ] && install -m 0700 -d "${bin_dir}"
[ ! -d "${man_dir}" ] && install -m 0700 -d "${man_dir}"

# Install jq binary
if [ -f "${tmp_dir}/${jq_binary}" ]; then
  printf '%s\n' "[INFO] Installing the jq binary"
  mv "${tmp_dir}/${jq_binary}" "${bin_dir}/jq"
  chmod 0700 "${bin_dir}/jq"
fi

# Install man page
printf '%s\n' "[INFO] Installing the jq man page"
curl -s -o "${man_dir}/${jq_man}" "${jq_man_url}"
chmod 0600 "${man_dir}/${jq_man}"

# VERSION CHECK
code_grn "[INFO] Done!"
code_grn "Installed Version: $(jq --version)"

# vim: ft=sh ts=2 sts=2 sw=2 sr et
